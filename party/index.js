window.addEventListener('load', async () => {

  /**
   * @type {Array<HTMLDivElement>}
   */
  const crowd = []

  const VERTICAL_COUNT = 7
  const HORIZONTAL_COUNT = 15
  const TOTAL_PEOPLE = HORIZONTAL_COUNT * VERTICAL_COUNT

  /**
   * 
   * @param {String} variable 
   * @param {Number} value 
   * @returns 
   */
  const setVariable = (variable, value) => 
    document.documentElement.style.setProperty(`--${variable}`, `${value}px`)

  /**
   * @param {Number} peep 
   * @returns {HTMLDivElement}
   */
  const createPeep = peep => {
    const element = document.createElement('div')
    element.classList.add('peep')
    element.classList.add(`peep-fileira${peep + 1}`)
    element.style.backgroundImage = `url(./peeps/peep-${Math.floor(Math.random() * TOTAL_PEOPLE + 1)}.png)`
    crowd.push(element)
    return element
  }

  /**
   * 
   * @param {HTMLDivElement} parent 
   */
  const generateCrowd = parent => new Promise((resolve) => {
    for (let i = 0; i < TOTAL_PEOPLE; i++) {
      createPeep(i)
    }

    for (let i = 0; i < VERTICAL_COUNT; i++) {
      for (let j = 0; j < HORIZONTAL_COUNT; j++) {
        const peep = crowd[i * HORIZONTAL_COUNT + j]
        peep.x = (i == 0 ? 8 * j : 10 * j) - (i % 2 == 0 ? 10 : 6.66) - i
        peep.y = (50 * i) - 50
        peep.style.left = `${peep.x}vw`
        peep.style.zIndex = `${VERTICAL_COUNT - i}`
        parent.appendChild(peep)

        if (Math.random() < 0.5) {
          peep.side = -1
          peep.style.transform = `scaleX(${peep.side})`
        }

        setInterval(() => {
          if (Math.random() < 0.5) {
            peep.side = peep.side === 1 ? -1 : 1
            peep.style.transform = `scaleX(${peep.side})`
          }
        }, 2000 + (Math.floor(Math.random() * 5 + 1) * 1000))
      }
    }

    resolve()
  })

  
  const play = document.querySelector('button')
  const selector = document.querySelector('select')
  const volume = document.querySelector('input')
  
  let spectrum
  function playSong(song) {
    if (spectrum)
      spectrum.stop()

    spectrum = new AudioSpectrum(`./${song}`)
    spectrum.volume(parseInt(volume.value))
    
    spectrum.loaded(async (context, start) => {
      await context.resume()
      spectrum.play()
      start()
    })
    
    spectrum.analyzeSpectrum((buffer, bufferLen) => {

      for (let i = 0; i < VERTICAL_COUNT; i++) {
        
        const min = (50 * i) - 200
        const max = min + 200

        for (let j = 0; j < HORIZONTAL_COUNT; j++) {
          const x = i * HORIZONTAL_COUNT + j
          setVariable(`jump${x + 1}`, Math.min(max, min + buffer[x])) // const scale = Math.min(buffer[x] / 110, 2.2) * 100
        }
      }
    })
  }

  play.onclick = () => playSong(selector.value)
  volume.onchange = () => spectrum.volume(parseInt(volume.value))

  await generateCrowd(document.querySelector('.peeps'))
})