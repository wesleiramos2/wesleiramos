// 1 segundo dividido por 20 frames
const FFT = 256;
const FPS = 1000 / 30;
const context = new AudioContext();

class AudioSpectrum {
	/**
	 * @param {String} file URL da música
	 * @param {Number} [fft=32] Padrão 32, isso vai retornar quantidade de ondas
	 * @param {Number} [fps=20] FPS da animação, padrão é 20 
	 */
	constructor(file, fft, fps) {
		this._file = file;
		this._analyze = null;
		this._load = null;
		this._ended = null;
		this._fps = fps || FPS;
		this._player = new Audio();
		this._source = context.createMediaElementSource(this._player);
		this._analyser = context.createAnalyser();
		this._source.connect(this._analyser);
		this._analyser.connect(context.destination);
		this._analyser.smoothingTimeConstant = 0.6;
		this._analyser.fftSize = fft || FFT;
		this._analyser.maxDecibels = 0;
	}

  get frequencyBinCount() {
    return this._analyser.frequencyBinCount;
  }

	_audioLoaded() {
		const fps = this._fps;
		const bufferLen = this._analyser.frequencyBinCount;
		let dataArray = new Uint8Array(bufferLen);

		let animFrame = () => {
			setTimeout(() => {
				requestAnimationFrame(animFrame);
			}, fps);

			this._analyser.getByteFrequencyData(dataArray);
			this._analyze(dataArray, bufferLen);
		};

		if (this._load) {
			this._load(context, () => {
				animFrame();
			});
		}
	}

	/**
	 * Define uma função que será chamada quando o audio
	 * for carregado
	 * @param {Function} callback 
	 */
	loaded(callback) {
		this._load = callback;
	}

	/**
	 * Define a função que será chamada quando o player parar de tocar
	 * @param {Function} callback 
	 */
	ended(callback) {
		this._ended = callback;
	}

	/**
	 * Define uma função que será chamada quando uma nova parte
	 * do espectro da musica for carregada
	 * @param {Function} callback 
	 */
	analyzeSpectrum(callback) {
		this._player.oncanplaythrough = () => {
			this._audioLoaded();
		};
		
		this._player.onended = () => {
			if (this._ended)
			  this._ended();
		};

		this._analyze = callback;
		this._player.src = this._file;
		this._player.load();
	}

	/**
	 * Toca o player
	 */
	play() {
		this._player.play();
	}

	/**
	 * Pausa o player
	 */
	pause() {
		this._player.pause();
	}

  /**
	 * Para o player por completo
	 */
	stop() {
		this._player.pause();
		this._player.currentTime = 0;
	}

	/**
	 * Altera o voluma do player
	 * @param {Number} v Valor entre 0 e 100 
	 */
	volume(value) {
		this._player.volume = value / 100;
	}

	/**
	 * Define se a musica irá repetir quando parar de tocar
	 * @param {Boolean} value 
	 */
	loop(value) {
		this._player.loop = value;
	} 

	/**
	 * Volume atual
	 */
	get currentVolume() {
		return this._player.volume * 100;
	}
}
